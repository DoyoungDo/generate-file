# 文件创建

## 创建 makrdown 引用的文件

> 当在markdown中写以下内容时，可以一键创建引用的文件（如果文件不存在的话）

```markdown
// 当前文件 README.md
[title](./custom/reference_file.md);
```

1. 将光标置到当前行任意位置
2. 右键选择`生成 markdown 引用文件`
3. 如果文件目标文件不存在则将会创建这个引用的文件

form
```
└─generate-file/
    ├─README.en.md
    └─README.md
```

to
```
└─generate-file/
    ├─README.en.md
    ├─README.md
    └─custom/
         └─reference_file.md
```