const hx = require('hbuilderx');

async function getActiveFileAndLineText() {
    const aditor = await hx.window.getActiveTextEditor()
    const document = aditor.document;
    
    const fileName = document.fileName;
    
    const line = await document.lineFromPosition(aditor.selection.anchor)
    
    return [fileName, line.text];
}

export {
    getActiveFileAndLineText
}